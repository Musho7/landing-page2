import { Button } from '../Button'
import './style.css'
export const CiberMonday = () => {
    return <div className = 'ciberMonday'>
        <div className = 'title'>
            <div>
                <p className = 'cyber'> <span>CYBER MONDAY</span> means an even <span>BIGGER SALE</span></p>
            </div>
            <div>
                <p className = 'text2'>Cyber Monday saves you money, WebWork saves you time.</p>
            </div>
            <div className = 'ciberMondayButton'>
                <Button title = {'SAVE BIG NOW'} />
            </div>
            <div className = 'text3'>
                <img alt = '#' src = {require('../../public/1.png')} />
                <p>14-day free trial    No credit card required</p>
            </div>
            <div className = 'text4'>
                <img alt = '#' src = {require('../../public/ratingstars.png')} />
                <p>Reviews from 51K+ happy users below and beyond</p>
            </div>
            <div className = 'text5'>
                <div>
                    <img alt = '#' src = {(require('../../public/1logo.png'))} /> 
                    <p></p>
                </div>
                <div>
                    <img alt = '#' src = {(require('../../public/2logo.png'))} /> 
                </div>
                <div>
                    <img alt = '#' src = {(require('../../public/3logo.png'))} /> 
                </div>
            </div>
        </div>
        <div className = 'ciberMondayIMg'>
            <img alt = '#' src = {require('../../public/50.png')} />
        </div>
    </div>
}
import { Button } from '../Button'
import './style.css'
export const Card = ({type,months,pracent,price,oldPrice,duration}) => {
    return <div className = 'card'>
        <div className ='outline'>
          <img alt = '#' src = {require('../../public/outline.png')} />
           <div className = 'procent'>
                <p className = 'pracentp'>{pracent}</p>
               <p>50%</p>
           </div>
        </div>
        <div className = 'info'>
            <div className = 'infoMonthly'>
                <p>{type}</p>
            </div>
            <div className ='infoprice'>
                <div className = 'oldPrice'>
                    <div className = 'oldPriceDiv'>
                        <p>{oldPrice}</p>
                        <div className = 'line'></div>
                    </div>
                   
                </div>
                <p>{price}</p>
                <p className ='per'>per user/month</p>
            </div>
            <div className= 'day'>
                <img alt = '#' src = {require('../../public/Vector.png')}/>
                <p>14-day Free Trial</p>
            </div>
            <div className  ='day'>
                <img alt = '#' src = {require('../../public/Vector.png')}/>
                <p>Access to All Features</p>
            </div>
            <div className = 'day'>
                <img alt = '#' src = {require('../../public/Vector.png')}/>
                <p>24/7 Support</p>
            </div>
            <div className = 'btndiv'>
                <Button title = {'GET YOUR DEAL'} />
            </div>
            <div className = 'duration'>
                <p>{duration}</p>
            </div>
        </div>
    </div>
}
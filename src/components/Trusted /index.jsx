import './style.css'
export const Trusted = () => {
    return <div className = 'trusted'>
        <p>Trusted by 1000+ Companies</p>
        <div className = 'trustedDiv'>
            <div>
                <img alt = '#' src = {require('../../public/r1.png')}></img>
            </div>
            <div> 
                <img alt = '#' src = {require('../../public/r2.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r3.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r4.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r5.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r6.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r7.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r8.png')}></img>
            </div>
            <div>
                <img alt = '#' src = {require('../../public/r9.png')}></img>
            </div>
            </div>
    </div>
}
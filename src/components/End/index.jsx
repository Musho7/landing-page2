import { Button } from '../Button'
import './style.css'
export const End = () => {
    return <div className = 'end'>
        <div className = 'end1'>
            <p>Save 50% Now</p>
            <Button title = 'GET MY DISCOUNT' />
        </div>
        <div className ='endImg'>
            <img alt = '#' src = {require('../../public/g.png')} />
        </div>
    </div>
}
import './style.css'
export const  Navbar = () => {
    return <div className = 'navbar'>
        <div className = 'navbarLogo'>
            <img alt = '#' src ={require('../../public/logo.png')} />
        </div>
        <div className = 'navbarButton'>
            <button >GET NOW</button>
        </div>
    </div>
}
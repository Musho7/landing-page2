import { Button } from '../Button'
import './style.css'
export const SaveBig =() => {
    return <div className = 'SaveBig'>
        <div className = 'SaveBigText'>
            <p className = 'SaveBigText1'>Save Big on All Features</p>
            <p className = 'SaveBigText2'>no exceptions</p>
        </div>
        <div className = 'table'>
            <div className = 'tableIconeDiv'>
                <div className = 'tableIcone'>
                    <img alt = '#' src = {require('../../public/icone1.png')}></img>
                    <p>Time Tracking with Screenshots</p>
                </div>
                <div className = 'tableIcone'>
                    <img alt = '#' src = {require('../../public/icone2.png')}></img>
                    <p>Productivity Monitoring</p>
                </div>
                <div className = 'tableIcone'>
                    <img alt = '#' src = {require('../../public/icone3.png')}></img>
                    <p style ={{width:'120px'}}>Task Management</p>
                </div>
                <div className = 'tableIcone'>
                    <img alt = '#' src = {require('../../public/icone4.png')}></img>
                    <p>Communication Channels</p>
                </div>
                <div className = 'tableIcone'>
                    <img alt = '#' src = {require('../../public/icone5.png')}></img>
                    <p>HR Tools</p>
                </div>
                <div className = 'tableIcone'>
                    <img alt = '#' src = {require('../../public/icone6.png')}></img>
                    <p>Online Reports</p>
                </div>
            </div>
            <div className = 'SaveBigLine' >
                <div className  ='SaveBigLine2' ></div>
            </div>


            <div className ='SaveBigInfo'>
                <div className = 'SaveBigInfoText'>
                    <p>Track the time your employees spend on work and get detailed information</p>
                    <div className = 'SaveBigInfovector'>
                        <img alt = '#' src = {require('../../public/Vector.png')} ></img>
                        <p>4 Screenshot Modes</p>
                    </div>
                    <div className = 'SaveBigInfovector'>
                        <img alt = '#' src = {require('../../public/Vector.png')} ></img>
                        <p>Idle Time Tracking</p>
                    </div>
                    <div className = 'SaveBigInfovector'>
                        <img alt = '#' src = {require('../../public/Vector.png')} ></img>
                        <p>Synchronized Tracking</p>
                    </div>
                    <div className = 'SaveBigInfovector'>
                        <img alt = '#' src = {require('../../public/Vector.png')} ></img>
                        <p>Billable Hours</p>
                    </div>
                    <div className = 'SaveBigInfoButton'>
                        <Button  title = 'RESERVE YOUR  SEST'/>
                    </div>
                </div>
                <div className = 'SaveBigInfoImg'>
                    <img alt = '#' src ={require('../../public/ttvisual.png')} />
                </div>
            </div>

        </div>
    </div>
}
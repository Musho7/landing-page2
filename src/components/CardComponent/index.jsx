import { Card } from '../Card'
import './style.css'
export const CardComponent = () => {
    return <div className  = 'CardComponent'>
        <div className  = 'CardComponentText'>
            <p>Benefit both ways - monthly or yearly</p>
        </div>
        <div className = 'cardDiv'>
            <Card type ={'Monthly'}  price = {'$ 2.49'} oldPrice ={'$4.99'} duration = {'*for 6 months'} />
            <Card type = {'Yearly'} pracent = {'18%+'}  price = {'$ 2.05'} oldPrice ={'$4.09'} />
        </div>
    </div>
}
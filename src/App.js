import './App.css';
import { CardComponent } from './components/CardComponent';
import { Cards } from './components/Cards';
import { CiberMonday } from './components/CyberMonday';
import { End } from './components/End';
import { Navbar } from './components/Navbar';
import { SaveBig } from './components/saveBig';
import { Trusted } from './components/Trusted ';

function App() {
  return (
    <div className="App">
      <div className = 'main'>
        <Navbar />
        <CiberMonday />
        <CardComponent />
        <Cards/>
        <SaveBig />
        <Trusted />
      </div>
      <div>
        <End />
      </div>
    </div>
  );
}

export default App;
